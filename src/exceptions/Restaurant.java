package exceptions;

import dish.Dish;
import dish.Spice;

import java.util.Random;

/**
 * Restaurant demo with control flow and error handling without using exceptions
 */
public class Restaurant {
  private static Random randomm=new Random();

  public static void main(String[] args) {

    Dish dish = new Dish(44, Spice.CURRY);
    if (!orderDish(15.0, dish)) return;
    eat(dish);

  }

  private static void eat(Dish dish) {
    System.out.println("Eating "+ dish);
  }

  private static boolean orderDish(double price, Dish dish) {
    if (checkBalance(price) < 0) return false;
    pay(price);
    return true;
  }

  private static void pay(double price) {
    System.out.println("Paying "+ price);
  }

  private static double checkBalance(double price) throws PaymentException{
    int balance = randomm.nextInt((int) (price*2));
    System.out.printf("Required €%.2f, balance €%d\n" , price ,balance);
    double remaining = balance - price;
    return remaining;
  }
}
