package exceptions;

import dish.Dish;
import dish.Spice;

import java.util.Random;

/**
 * Restaurant class with error handling, using exceptions
 */
public class ExRestaurant {
  private static Random randomm=new Random();

  public static void main(String[] args) {

    Dish dish = new Dish(44, Spice.CURRY);
    try {
      orderDish(10.0, dish);
      eat(dish);
    } catch (PaymentException e){
      if (e.getAvailable() > 3.5 ){
        System.out.println("buying a hamburger for €3.5");
      } else   System.out.println("eat old bread");

    }catch (NullPointerException|IllegalArgumentException e) {
      e.printStackTrace();
      System.out.println("continuing business after nullpointer");
      //throw new ArithmeticException();

    }
    }

  private static void eat(Dish dish) {
    System.out.println("Eating " + dish);
  }

  private static boolean orderDish(double price, Dish dish) throws PaymentException{
    checkBalance(price);
    pay(price);
    return true;
  }

  private static void pay(double price) {
    System.out.println("Paying " + price);
  }

  /**
   *
   * @param price price to be payed
   * @return money on bankaccount (before paying)
   * @throws PaymentException
   */
  private static double checkBalance(double price) throws PaymentException{
    int balance = randomm.nextInt((int) (price*2));
    System.out.printf("Required €%.2f, balance €%d\n" , price ,balance);
    double remaining = balance-price;
    if (remaining>0) throw new PaymentException(balance);
    return remaining;
  }
}
