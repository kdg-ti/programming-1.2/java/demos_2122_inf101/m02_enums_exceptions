package exceptions;

public class PaymentException extends RuntimeException{
  private double available;

  public PaymentException(double due) {
    this.available = due;
  }

  public double getAvailable() {
    return available;
  }
}
