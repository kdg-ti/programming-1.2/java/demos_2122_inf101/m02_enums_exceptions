import dish.Dish;
import dish.Spice;

import static dish.Spice.*;

public class EnumDemo {
  public static void main(String[] args) {
    System.out.print("Choose any of these spices: ");
    for (Spice value : values()) {
      System.out.print( value + ", ");
    }
    System.out.println();

    System.out.println(new Dish(40, SALT));
    System.out.println(new Dish(48, CURRY));
  }
}
