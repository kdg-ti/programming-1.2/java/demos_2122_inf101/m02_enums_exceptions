import dish.Dish;
import dish.Spice;

import java.util.Map;

public class MapEntry {
  public static void main(String[] args) {

    Map.Entry<Double, Dish> entry = Map.entry(15.0, new Dish(45, Spice.CURRY));
    System.out.println(entry);
    Dish aDisdh = new Dish( 4, Spice.CURRY){
      int price;
      int getPrice(){
        return price;
      }
      void setPrice(int price){
        this.price=price;
      }
    };
  }
}
