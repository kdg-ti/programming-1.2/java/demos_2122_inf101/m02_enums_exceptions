package dish;

/**
 * A java class that uses constants to represent a discrete set of values (spices)
 */
public class SpiceConstant {
  public final static int SALT = 1;
  public final static int PEPPER = 2;
  public final static int GINGER = 3;
  public final static int CURRY = 4;

}
