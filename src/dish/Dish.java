package dish;

public class Dish {
  private int preparationTime;
  private Spice spice;

  public Dish(int preparationTime, Spice spice) {
    this.preparationTime = preparationTime;
    this.spice = spice;
    if (spice == Spice.SALT) System.out.println("This is not a healthy dish");
    if (spice.getScore() > 5) System.out.println(spice.name() + " is hot");
  }

  public int getPreparationTime() {
    return preparationTime;
  }

  public void setPreparationTime(int preparationTime) {
    this.preparationTime = preparationTime;
  }

  public Spice getSpice() {
    return spice;
  }

  public void setSpice(Spice spice) {
    this.spice = spice;
  }

  @Override
  public String toString() {
    return "Dish{" +
      "preparationTime=" + preparationTime + " minutes " +
      ", spice='" + spice + '\'' +
      '}';
  }
}
