package dish;

public enum Spice {
  SALT(3),
  PEPPER(7),
  GINGER(5) ,
  CURRY(8);

  /**
   * hotness score for a Spice
   */
 private final int score;

  private Spice(int score) {
    this.score = score;
  }



  public int getScore() {
    return score;
  }

  @Override
  public String toString() {
    return "Spice{" +
      "score=" + score +
      ", name='" + name() + '\'' +
      ", ordinal=" + ordinal() +
      "} " + super.toString();
  }
}
